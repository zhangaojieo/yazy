package cn.yazy.异常;
/**
 * 自定义异常(运行时异常) 继承 Exception
 *@author ZhangAJ:
 *@version 创建时间：2022年10月10日下午2:50:12
 */
public class MyRunTimeException extends RuntimeException {
    //无参构造方法
    public MyRunTimeException() {
        super();
        // TODO Auto-generated constructor stub
    }
    //有参构造方法
    public MyRunTimeException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }




}
