package cn.yazy.异常;
/**
 * 异常的捕获处理:
 * 2.throws 异常捕获
 * 在自定义的方法体中如果有编译时异常需要预处理,可以捕获处理,也可以抛出处理
 * 处理异常时,使用throws抛出处理
 * 1.谁调用这个方法,谁负责处理这个异常
 * 2.在定义方法,把异常抛出去就是为了提醒方法的使用者,有异常需要预处理
 *
 * 处理异常时:
 * 1.一般情况下在调用其他方法时,如果被调用的方法有编译时异常需要预处理,选择捕获处理.
 * 因为你调用了这个方法,就需要负责处理
 *
 * 2.在定义方法时,如果方法体中有编译异常需要处理可以选择捕获,也可以选择抛出.
 * 如果在方法体中用throws语句抛出一个异常对象所在的方法应该使用throws声明对象
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年9月29日上午9:56:41
 */
public class ExceptionDemo2 {

    public static void main(String[] args) {

        try {
            testException();
        } catch (ClassNotFoundException e) {

            e.printStackTrace();
        }
    }

    public static void testException() throws ClassNotFoundException {

        Class.forName("com.mysql.jdbc.Driver");

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {

            e.printStackTrace();
        }

    }

}
