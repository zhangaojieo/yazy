package cn.yazy.异常;
/**
 * 3.throw 关键字
 * throw用来对外主动抛出一个异常，通常下面俩种情况我们主动对外抛出异常:
 * 1.当程序遇到应该满足语法,但是不满足业务要求时,可以抛出一个异常告知调用者.
 * 2.当程序执行遇到一个异常,但是该异常不应当在当代代码片段被解决时可以抛出给调用者.
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年10月6日上午8:28:48
 */
public class ExceptionDemo3 {

    public static void main(String[] args) {
        Person p = new Person();
        p.setAge(100000);//符合语法,但是不符合业务逻辑要求
        System.out.println("此人年龄:"+p.getAge());
    }
}
/**
 * 测试异常的抛出
 */
class Person{

    private int age;

    public int getAge(){
        return age;
    }
    public void setAge(int age) {
        if (age<0||age>100) {
            //使用throw对外抛出应该异常
            throw new RuntimeException("年龄不合法");
        }
        this.age = age;
    }
}
