package cn.yazy.异常;

import java.awt.AWTException;
import java.io.IOException;
import java.sql.SQLDataException;

/**
 * 子类重写超类含有throws声明异常抛出的方法时对throws的几种特殊的重写规则
 *@author ZhangAJ:
 *@version 创建时间：2022年10月6日上午9:39:10
 */
public class ExceptionDemo4 {

    //throws 可以抛出一个或多个类
    public void dosome() throws IOException,AWTException{}


}
//子类
class SubClass extends ExceptionDemo4{
    //规则1:不可以抛出超类方法抛出异常的超类异常
    //public void dosome() throws Exception{}

    //规则2:不允许抛出额外异常(超类方法中没有的,并且没有继承关系的异常)
    //public void dosome() throws SQLDataException{}

    //规则3:
}
