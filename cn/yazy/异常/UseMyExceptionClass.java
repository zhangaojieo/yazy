package cn.yazy.异常;
/**
 * 使用自定义异常
 *@author ZhangAJ:
 *@version 创建时间：2022年10月10日下午2:52:18
 */
public class UseMyExceptionClass {

    public static void main(String[] args) {
        //testException1();
        testException2();
    }

    //使用MyChenckException异常
    private static void testException1(){
        try {
            method1(10,0);
        } catch (MyChenckException e) {
            /*
             * 必须拦截,拦截后给出拦截的意见,如果不给出处理就属于隐藏了该异常,
             * 系统将不给出任何提示,使程序调试非常困难
             */
            //e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    //编译时异常使用案例
    public static void method1(int value1,int value2) throws MyChenckException{
        if (value2 == 0) {
            throw new MyChenckException("方法1:自定义编译异常,除数不能为0");
        }

        int value3 = value1 /value2;

        System.out.println(value3);
    }

    //使用MyRunTimeException异常
    private static void testException2() {
        method2(10,0);
    }
    //运行时异常使用案例
    public static void method2(int value1,int value2){
        if (value2 == 0) {
            //抛出运行时异常,可以不用throws进行声明,但也可以显示声明
            throw new MyRunTimeException("方法1:自定义编译异常,除数不能为0");
        }

        int value3 = value1 /value2;

        System.out.println(value3);
    }
}
