package cn.yazy.recursion;

/*
    递归算法和执行流程
 * @author: ZhangAJ
 * @create: 2022年11月17日 9:48
 */
public class RecursionDemo2 {
    public static void main(String[] args) {
        System.out.println(f(5));
    }
    public static int f(int n){
        if(n == 1 ){
            return 1;
        }else{
            return f(n - 1) * n;
        }
    }
}
