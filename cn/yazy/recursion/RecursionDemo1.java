package cn.yazy.recursion;

/*
    方法的递归：
        递归算法
     方法递归是直接调用自己或者间接调用自己的形式称为方法递归，
    递归做为一种算法在程序设计语言中被广泛使用。
      直接递归: 方法自己调用自己
      间接递归: 方法调用其他方法，其他方法又回调方法自己
     方法递归所存在的问题：
        递归如果没有控制好终止，会出现递归死循环，导致栈内存溢出现象。
 * @author: ZhangAJ
 * @create: 2022年11月17日 9:40
 */
public class RecursionDemo1 {
    public static void main(String[] args) {
        //test();
        test2();
    }

    public static void test(){
        System.out.println("======test被执行======");
        test();//直接递归
    }

    public static void test2(){
        System.out.println("======test2被执行======");
        test3();//间接递归
    }
    public static  void  test3(){
        System.out.println("======test3被执行======");
        test2();
    }
}
