package cn.yazy.recursion;

/**
 * @author: ZhangAJ
 * @create: 2022年11月17日 9:57
 */
public class TestRecursionDemo {

    /*
            课堂练习：
              目的：1-n求和
              猴子第一天摘下若干桃子，当即吃了一半，觉得好不过瘾，于是又多吃了一个，
              第二天又吃前天剩余桃子数量的一半，觉得好不过瘾，于是又多吃了一个，以后
              每天都是吃前天剩余桃子数量的一半，觉得好不过瘾，又多吃了一个，等到第10
              天的时候发现桃子只有1个了。
              需求：
                请问猴子第一天摘了多少个桃子？
         */
    public static void main(String[] args) {
        int i;
        int j =1;//第10天的剩余 1个
        for(i=9;i>=1;i--){
        j = (j+1)*2;
    }
        System.out.println("猴子第一天总共摘了："+j+"个桃子");//1534个
    }
}