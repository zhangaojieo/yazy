package cn.yazy.scoket.ScoketDemo1;

import java.io.IOException;
import java.net.Socket;
/**
 * 聊天室客户端
 * @author: ZhangAJ
 * @create: 2022年11月28日 15:16
 */
public class Client {
    /*
    java.net.Socket 套接字
    Socket封装了TCP协议的通讯细节，我们通过它可以与远端计算机建立链接，
    并通过它获取两个流(一个输入，一个输出)，然后对两个流的数据读写完成
    与远端计算机的数据交互工作。
    我们可以把Socket想象成是一个电话，电话有一个听筒(输入流)，一个麦克
    风(输出流)，通过它们就可以与对方交流了。
    */
    private Socket socket;
    /**
     * 构造方法，用来初始化客户端
     */
    public Client(){
        try {
            System.out.println("正在链接服务端...");
            /*
            实例化Socket时要传入两个参数
            参数1:服务端的地址信息
            可以是IP地址，如果链接本机可以写"localhost"
            参数2:服务端开启的服务端口
            我们通过IP找到网络上的服务端计算机，通过端口链接运行在该机器上
            的服务端应用程序。
            实例化的过程就是链接的过程，如果链接失败会抛出异常:
            java.net.ConnectException: Connection refused: connect
            */
            socket = new Socket("localhost",8088);
            System.out.println("与服务端建立链接!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 客户端开始工作的方法
     */
    public void start(){
    }
    public static void main(String[] args) {
        Client client = new Client();
        client.start();
    }
}