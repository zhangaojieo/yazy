package cn.yazy.scoket.ScoketDemo5;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

/**
 * 聊天室服务端
 *   服务端转发消息给所有客户端
 * @author: ZhangAJ
 * @create: 2022年12月15日 9:22
 */
public class Server {
    /**
     *  运行在服务端的ServerSocket主要完成两个工作:
     *  1:向服务端操作系统申请服务端口，客户端就是通过这个端口与ServerSocket建立链接
     *  2:监听端口，一旦一个客户端建立链接，会立即返回一个Socket。通过这个Socket
     *  就可以和该客户端交互了
     *
     *  我们可以把ServerSocket想象成某客服的"总机"。用户打电话到总机，总机分配一个
     *  电话使得服务端与你沟通。
     */
    private ServerSocket serverSocket;

    /*
        存放所有客户端输出流，用于广播消息
    */
    private PrintWriter[] allOut = {};

    /**
     * 服务端构造方法，用来初始化
     */
    public Server() {
        try {
            System.out.println("正在启动服务端...");
            /*
             * 实例化ServerSocket时要指定服务端口，该端口不能与操作系统其他
             * 应用程序占用的端口相同，否则会抛出异常:
             * java.net.BindException:address already in use
             * 端口是一个数字，取值范围:0-65535之间。
             * 6000之前的的端口不要使用，密集绑定系统应用和流行应用程序。
             * */
            serverSocket = new ServerSocket(8088);
            System.out.println("服务端启动完毕!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 服务端开始工作的方法
     */
    public void start() {
        try {
            while(true){
                System.out.println("等待客户端链接...");
                /*
                 * ServerSocket提供了接受客户端链接的方法:
                 * Socket accept()
                 * 这个方法是一个阻塞方法，调用后方法"卡住"，此时开始等待客户端
                 * 的链接，直到一个客户端链接，此时该方法会立即返回一个Socket实例
                 * 通过这个Socket就可以与客户端进行交互了。
                 * 可以理解为此操作是接电话，电话没响时就一直等。
                 * */
                Socket socket = serverSocket.accept();
                System.out.println("一个客户端链接了！");
                //启动一个线程与该客户端交互
                ClientHandler clientHandler = new ClientHandler(socket);
                Thread t = new Thread(clientHandler);
                t.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }
    /*定义线程任务，目的是让一个线程完成与特定客户端的交互工作*/
    private class ClientHandler implements Runnable{
        private Socket socket;
        private String host;//记录客户端的IP地址信息

        public ClientHandler(Socket socket) {
            this.socket = socket;
            //通过socket获取远端计算机地址信息
            host = socket.getInetAddress().getHostAddress();
        }
        @Override
        public void run() {
            try {
                /*
                 * Socket提供的方法:
                 * InputStream getInputStream()
                 * 获取的字节输入流读取的是对方计算机发送过来的字节
                 * */
                InputStream in = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(in,"UTF-8");
                BufferedReader br = new BufferedReader(isr);

                //服务端发送消息
                OutputStream out = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(out,"UTF-8");
                BufferedWriter bw = new BufferedWriter(osw);
                PrintWriter pw = new PrintWriter(bw,true);

                //将该输出流存入共享数组allOut中
                //1对allOut数组扩容
                allOut = Arrays.copyOf(allOut,allOut.length+1);
                //2将输出流存入数组最后一个位置
                allOut[allOut.length-1] = pw;


                String message = null;
                while((message = br.readLine())!=null) {
                    System.out.println(host+"说:" + message);
                    //将消息回复给所有客户端
                    for(int i=0;i<allOut.length;i++) {
                        allOut[i].println(host + "说:" + message);
                    }
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}