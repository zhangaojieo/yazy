package cn.yazy.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 算一算从出生到现在生存了多少天
 * 分析:
 *     1.从键盘录入你的出生的年月日
 *     2.把录入的字符串转为日期
 *     3.通过该日期获得一个毫秒值
 *     4.获取当前时间的毫秒值
 *     5.用当前日期毫秒值减去录入日期的毫秒值
 *     6.把计算后的毫秒值转为天即可
 *@author ZhangAJ:
 *@version 创建时间：2022年10月13日上午9:44:26
 */
public class Test_DateDemo {

    public static void main(String[] args) throws ParseException {

        String Birthday="2002-10-11";//出生日期
        SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

        Date BirthdayDate = SDF.parse(Birthday);//出生日期Date对象
        Date NowDate = new Date();//当前日期Date对象

        long BirthdayTime = BirthdayDate.getTime();//出生日期的毫秒值
        long NowTime = NowDate.getTime();//当前日期的毫秒值

        long Time = (NowTime-BirthdayTime)/1000/60/60/24;//转换天数
        System.out.println("出生到现在生存了"+Time+"天");

    }

}
