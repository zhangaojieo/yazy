package cn.yazy.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Date -> String(格式化)
 *     public final String format(Date date)
 *
 * String(格式化) -> Date
 *     public Date parse(String source)
 *
 * DateFormat:可以进行日期和字符串的格式化和解析.但是由于是抽象类
 * 所以要实现具体的格式化功能就是必须使用其子类SimpleDateFormat
 *
 * SimpleDateFormat的构造方法:
 *      SimpleDateFormat() 无参构造
 *      SimpleDateFormat(String patter) 给定模式
 *
 * 这个模式字符串该如何去写?
 * 通过观察API文档 我们找到对应模式
 *
 * 年y 月M 日d 时H 分m 秒s
 *
 * 2022年10月13日09:11:18
 *
 * yyyy-MM-dd HH:mm:ss
 *@author ZhangAJ:
 *@version 创建时间：2022年10月13日上午8:56:17
 */
public class DateFormatDemo {

    public static void main(String[] args) throws ParseException {

        //Date ->String
        //创建日期对象
        Date date = new Date();

        //创建格式化对象
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH:mm:ss");
        String s = sdf.format(date);//格式转换
        System.out.println(s);//2022年10月13日09:28:52


        //String -> Date
        String s1 = "2022-10-13 09:31:07";
        //把一个字符串解析为日期的时候,请注意格式必须和给定的字符串格式搭配
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dd = sdf2.parse(s1);//格式转换 会报出编译异常 选择抛出 或try-catch处理
        System.out.println(dd);//Thu Oct 13 09:31:07 CST 2022






    }

}
