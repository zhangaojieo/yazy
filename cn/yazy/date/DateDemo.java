package cn.yazy.date;

import java.util.Date;

/**
 * Date Java中提供了一个用于处理时间的类
 *
 * java.util.Date
 *
 * Date:表示特定的时间,可以精确到毫秒
 * 构造方法:
 *     Date():根据当前的默认毫秒值创建日期对象
 *     Date(long date):根据给定的毫秒值创建日期对象
 *@author ZhangAJ:
 *@version 创建时间：2022年10月13日上午8:25:58
 */
public class DateDemo {

    public static void main(String[] args) {

        //创建对象1
        Date date = new Date();
        //date:Thu Oct 13 08:33:44 CST 2022
        System.out.println("date:"+date);

        //获取从1970年1月1日0时0分1秒到此时的毫秒数
        long time = System.currentTimeMillis();
        System.out.println(time);//1665621490478
        //创建对象2
        Date date2 = new Date(time);
        //date:Thu Oct 13 08:38:10 CST 2022
        System.out.println("date:"+date2);


        //获取获取从1900年到今年共有多少年 getYear()
        int year = date2.getYear();
        System.out.println(year);//122

    }

}
