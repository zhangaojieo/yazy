package cn.yazy.date;

import java.util.Calendar;
import java.util.Date;

/**
 * 日历类(抽象类) Calendar
 * java.util.Calendar,在Date之后出现,替换了许多Date的方法
 * 该类将所有可能用到的时间信息封装为静态成员变量,方便获取
 * 日历类就是方便获取各属性包括年 月 日 星期 时区等信息
 *
 * 创建对象:
 * 由于Calendar是抽象类,Calendar类在创建对象时并非之间创建
 * 而是通过静态方法创建返回其子类对象GregorianCalendar
 *
 * 常用方法:
 * public final Date getTime() 在日历中获取Date对象
 * public final void setTime() 将Date对象转换为Calendar
 * public int get(int field) 返回指定的日历字段值
 *@author ZhangAJ:
 *@version 创建时间：2022年10月17日下午3:00:08
 */
public class CalendarDemo {
    public static void main(String[] args) {

        //获得Calendar实例对象
        Calendar cal = Calendar.getInstance();
        System.out.println(cal);

        //可以从cal获取一个日期对象
        Date date = cal.getTime();
        System.out.println(date);//Mon Oct 17 15:22:11 CST 2022

        //将日期转换为Calendar,因为calendar并没有提供对应的构造方法
        Date date2 = new Date(1000);
        cal.setTime(date2);
        System.out.println(cal);

        //获得Calendar实例对象
        Calendar cal2 = Calendar.getInstance();
        //获得年
        int year = cal.get(Calendar.YEAR);
        //获得月
        int month = cal.get(Calendar.MONTH)+1;
        //获得日
        int day = cal.get(Calendar.DATE);
        System.out.println(year+"年"+month+"月"+day+"日");//2002年10月11日

        //获得周的第几天
        int num = cal2.get(Calendar.DAY_OF_WEEK)-1;
        System.out.println(num);//1

        //设置年
        cal2.set(Calendar.YEAR, 2008);
        //Fri Oct 17 15:34:44 CST 2008
        System.out.println(cal2.getTime());

        //设置年月日
        cal2.set(2008,8,8);
        //Mon Sep 08 15:35:33 CST 2008
        System.out.println(cal2.getTime());

        //将当前日历往后推一年
        cal2.add(Calendar.YEAR,1);
        //Tue Sep 08 15:37:33 CST 2009
        System.out.println(cal2.getTime());

        //将当前日历往前推俩天
        cal2.add(Calendar.DAY_OF_MONTH,2);
        //Thu Sep 10 15:38:59 CST 2009
        System.out.println(cal2.getTime());

        /**
         * 备注:西方星期 周日为本周第一天,中国为周一
         * 在Calendar类中，月份表示0-11 代表1-12月
         * 日期是有大小关系的，时间靠后，时间越大
         */

    }

}
