package cn.yazy.date;

import java.util.Scanner;

/**
 * 课后练习:
 * 1.给定一个长度,随机产生一个该长度的字符串
 * 要求生成的字符串应该由大写、小写字母以及数字组成
 *
 * 2.从键盘接收一段字符串,判断该字符串是否为回文
 * 回文:(上海自来水来自海上)
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年10月17日下午3:48:33
 */
public class HomeWorkDemo1 {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        System.out.println("输入一个字符串： ");
        String s=input.nextLine();
        int low=0;
        int high=s.length()-1;
        boolean isPalindrome=true;
        while(low<high){
            if(s.charAt(low)!=s.charAt(high)){
                isPalindrome=false;
                break;
            }
            low++;
            high--;
        }
        if(isPalindrome){
            System.out.println("是回文");
        }else{
            System.out.println("不是回文");
        }
    }

}
