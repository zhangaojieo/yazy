package cn.yazy.date;

import java.util.Date;

/**
 *@author ZhangAJ:
 *@version 创建时间：2022年10月13日上午8:45:19
 */
public class DateDemo2 {

    public static void main(String[] args) {

        //public long getTime();获取时间,以毫秒为单位
        //public void setTime();设置时间

        //以Date得到一个毫秒值 getTime()
        //把一个毫秒值转换为Date setTime(long time)

        Date date = new Date();


        //获取时间
        long time = date.getTime();
        System.out.println(time);
        //getTime() == System.currentTimeMillis() 效果相同
        System.out.println(System.currentTimeMillis());


        System.out.println("firsDate:" + date);
        //设置时间
        date.setTime(3000);
        System.out.println("afterDate:" + date);


    }

}
