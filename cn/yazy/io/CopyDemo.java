package cn.yazy.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/*
    文件复制
 * @author: ZhangAJ
 * @create: 2022年11月24日 8:40
 */
public class CopyDemo {
    public static void main(String[] args) throws IOException {
        // 创建文件输入流读取原文件
        FileInputStream fis = new FileInputStream("C:/Users/DELL/Desktop/001.jpeg");
        // 创建文件输出流写复制文件
        FileOutputStream fos = new FileOutputStream("C:/Users/DELL/Desktop/001_cp.jpeg");

        /*
            原文件的数据：
               10111100 00111111 10100101  10101010......
               ^^^^^^^^
               d = fis.read()

               d：00000000 00000000 00000000 10111100
                                             ^^^^^^^^
               fos.write(d)
               复制文件
               10111100
               ^^^^^^^^
         */
        int d;//用来保存每次读取到复制的字节内容
        long start = System.currentTimeMillis();
        //读写操作 边读边写
        while((d = fis.read())!= -1){
            fos.write(d);
        }
        long end = System.currentTimeMillis();
        System.out.println("复制完毕！耗时："+(end-start)+"ms");
        fis.close();
        fos.close();
    }

}