package cn.yazy.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * JAVA IO Input&Output 输入和输出
 * java程序与外界交换数据是基于IO完成的，这里输入与输出一个负责读一个负责写
 * 输入:是从外界到我们写的程序的方向，是用来从外界获取信息的。因此是"读"操作
 * 输出:是从我们写的程序到外界的方向，是用来向外界发送信息的。因此是"写"操作
 *
 * java将IO比喻为"流",可以理解为是程序与外界连接的"管道",内部流动的是字节，并且
 * 字节是顺着同一侧方向顺序移动的。
 *
 * java.io.InputStream 输入流，这个类是所有字节输入流的超类，规定了所有字节输入
 * 流读取数据的相关方法。
 * java.io.OutputStream 输出流，这个类是所有字节输出流的超类，规定了所有字节输出
 * 流写出数据的相关方法。
 *
 * 实际应用中，我们连接不同的设备，java都专门提供了用于连接它的输入流与输出流，而
 * 这些负责实际连接设备的流称为节点流，也叫低级流。是真实负责读写数据的流。
 * 与之对应的还有高级流，高级流可以连接其他的流，目的是当数据流经它们时，对数据做某
 * 种加工处理，用来简化我们的操作。
 *
 *
 * 文件流
 * java.io.FileInputStream和FileOutputStream
 * 这是一对低级流，继承自InputStream和OutputStream。用于读写硬盘上文件的流
 *
 * @author: ZhangAJ
 * @create: 2022年11月21日 15:16
 */
public class FOSDemo {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        //向当前目录下的demo.dat文件中写入数据
        /*
            FileOutputStream提供的常用构造器：
                FileOutputStream(File file) throws FileNotFoundException
                FileOutputStream(String name) throws FileNotFoundException
           注意： IO流会牵扯到多个异常，需要手动抛出或try-catch
         */
        //文件流创建时，如果该文件不存在会自动将其创建
        // （前提是该文件的所在目录必须存在）
        //
        FileOutputStream fos =
                new FileOutputStream("./demo.dat");
        /*
            void write(int b) throws IOException
            向文件中写入1个字节，写入的内容是给定的int值对应的2进制的“低八位”

            int值 1：
            二进制：00000000 00000000 00000000 00000001
            demo.dat文件中的内容：
                00000001
         */
        fos.write(1);

        /*
            int值 2：
            二进制：00000000 00000000 00000010
              demo.dat文件中的内容：
                00000001  00000010
         */
        fos.write(2);

        //关闭流资源
        fos.close();
        System.out.println("执行完了！");


    }

}