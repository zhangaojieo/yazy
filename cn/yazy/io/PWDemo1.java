package cn.yazy.io;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * 缓冲字符流
 * java.io.BufferedWriter和BufferedReader
 * 缓冲字符流内部也有一个缓冲区,读写文本数据以块读写形式加快效率.并且缓冲流有一个特别的
 * 功能:可以按行读写文本数据.
 *
 * java.io.PrintWriter具有自动行刷新的缓冲字符输出流,实际开发中更常用.它内部总是会自
 动
 * 连接BufferedWriter作为块写加速使用.
 * @author: ZhangAJ
 * @create: 2022年11月26日 15:49
 */
public class PWDemo1 {
    public static void main(String[] args)
            throws FileNotFoundException,
            UnsupportedEncodingException {
        //向文件pw.txt中写入文本数据
        /*
        PW直接提供了方便向文件中写入数据的构造方法
        PrintWriter(String fileName)
        PrintWriter(File file)
        */
        PrintWriter pw = new PrintWriter("pw.txt","UTF-8");
        pw.println("一给我嘞giao!giao!giao!");
        pw.println("奥力给!");
        System.out.println("写出完毕!");
        pw.close();
    }
}