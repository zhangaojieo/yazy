package cn.yazy.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 使用java.io.BufferedReader按行读取文本数据
 * 缓冲字符输入流:java.io.BufferedReader
 * 是一个高级的字符流，特点是块读文本数据，并且可以按行读取字符串。
 * @author: ZhangAJ
 * @create: 2022年11月26日 15:44
 */
public class BRDemo {
    public static void main(String[] args) throws IOException {
        //将当前源程序读取出来并输出到控制台上
        FileInputStream fis = new FileInputStream(
                "./src/cn/yazy/io/BRDemo.java");
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        String line;
        /*
        BufferedReader提供了一个读取一行字符串的方法:
        String readLine()
        该方法会返回一行字符串，返回的字符串不含有最后的换行符。
        当某一行是空行时(该行内容只有一个换行符)则返回值为空字符串。
        如果流读取到了末尾，则返回值为null。
        */
        int count=0;
        while((line = br.readLine()) != null) {
            System.out.println(line);
            count++;
        }
        br.close();
        System.out.println("这个文件总共"+count+"行");
    }
}
