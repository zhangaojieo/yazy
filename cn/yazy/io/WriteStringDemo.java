package cn.yazy.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/*
写文本数据
    String提供方法:
    byte[] getBytes(String charsetName)
    将当前字符串转换为一组字节
    参数为字符集的名字，常用的是UTF-8。 其中中文字3字节表示1个，英文1字节表示1个。
 * @author: ZhangAJ
 * @create: 2022年11月26日 14:23
 */
public class WriteStringDemo {
    public static void main(String[] args) throws IOException {
        /**
         * 向文件中写入文本数据
         */
        FileOutputStream fos = new FileOutputStream("demo.txt");
        String str = "呕吼 super idol的笑容都没你的甜,";
        /*
        支持中文的常见字符集有:
        GBK:国标编码。英文每个字符占1个字节，中文每个字符占2个字节
        UTF-8:内部是unicode编码，在这个基础上不同了少部分2进制信息作为长度描述
        英文每个字符占1字节
        中文每个字符占3字节
        String提供了将字符串转换为一组字节的方法
        byte[] getBytes(String charsetName)
        参数为字符集的名字，名字不缺分大小写，但是拼写错误会引发异常:
        UnsupportedEncodingException
        不支持 字符集 异常
        */
        byte[] data = str.getBytes("UTF-8");
        fos.write(data);
        fos.write("八月正午的阳光，都没你耀眼。".getBytes("UTF-8"));
        System.out.println("写出完毕!");
        fos.close();
    }
}