package cn.yazy.io;

import java.io.*;
import java.util.Arrays;

/*
对象流
    java.io.ObjectOutputStream和ObjectInputSteam
    对象流是一对高级流，在流连接中的作用是进行对象的序列化与反序列化。
    对象序列化:将一个java对象按照其结构转换为一组字节的过程
    对象反序列化:将一组字节还原为java对象(前提是这组字节是一个对象序列化得到的字节)
 * @author: ZhangAJ
 * @create: 2022年11月26日 15:00
 */
public class OOSDemo {
    public static void main(String[] args) throws IOException {
        //将一个Person对象写入文件person.obj
        String name = "苍#null";
        int age = 18;
        String gender = "女";
        String[] otherInfo = {
                "是一名台词不多的演员","来自岛国","爱好写大字",
                "广大男性同胞的启蒙老师"};
        Person p = new Person(name,age,gender,otherInfo);
        System.out.println(p);
        FileOutputStream fos = new FileOutputStream("person.obj");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(p);
        System.out.println("写出完毕!");
        oos.close();
    }

}

class Person implements Serializable {
    /*序列化编号*/
    private static final long serialVersionUID = 1L;

    String name;
    int age;
    String gender;
    /*
     当一个属性被关键字transient修饰后，这个对象在序列化时会忽略这个属性的值。
     忽略不必要的属性可以达到对象序列化的"瘦身"操作，减少资源开销。
    */
    transient String[] otherInfo;//其他信息

    public Person() {
    }

    public Person(String name, int age, String gender, String[] otherInfo) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.otherInfo = otherInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String[] getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String[] otherInfo) {
        this.otherInfo = otherInfo;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", otherInfo=" + Arrays.toString(otherInfo) +
                '}';
    }
}