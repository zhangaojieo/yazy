package cn.yazy.io;

import java.io.*;

/**
 * 在流链接中使用PW
 * @author: ZhangAJ
 * @create: 2022年11月26日 15:49
 */
public class PWDemo2 {
    public static void main(String[] args) throws FileNotFoundException,
            UnsupportedEncodingException {
        FileOutputStream fos = new FileOutputStream("pw2.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos,"UTF-8");
        BufferedWriter bw = new BufferedWriter(osw);
        PrintWriter pw = new PrintWriter(bw);
        pw.println("夜空中最亮的星，能否听清。");
        pw.println("那仰望的人心底的孤独和叹息。");
        System.out.println("写出完毕!");
        pw.close();
    }
}
