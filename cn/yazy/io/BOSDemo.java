package cn.yazy.io;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/*
    缓冲输出流写出数据时的缓冲区问题
    通过缓冲流写出的数据会被临时存入缓冲流内部的字节数组,
  直到数组存满数据才会真实写出一次
 * @author: ZhangAJ
 * @create: 2022年11月26日 14:43
 */
public class BOSDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("bos.txt");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        String line = "画画的baby,画画的baby,奔驰的小野马和带刺的玫瑰~";
        byte[] data = line.getBytes("UTF-8");
        bos.write(data);
        /*
        void flush()
        将缓冲流的缓冲区中已缓存的数据一次性写出。
        注:频繁调用该方法会提高写出的次数降低写出效率。但是可以换来写出
        数据的即时性。
        */
        bos.flush();
        System.out.println("写出完毕!");
        /*
            缓冲流的close操纵中会自动调用一次flush,确保所有缓存的数据会被写出
        */
        bos.close();
        /*
            flush方法是被定义在了输出流的超类上了，这意味着所有的输出流都有flush
        方法，目的是在流连接中传递flush操作给缓冲输出流。
        其他高级流的flush中只是调用了它链接的流的flush.
         */
    }
}
