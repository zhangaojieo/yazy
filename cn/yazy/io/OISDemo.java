package cn.yazy.io;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * 使用对象输入流完成对象的反序列化
 * @author: ZhangAJ
 * @create: 2022年11月26日 15:13
 */
public class OISDemo {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //从person.obj文件中将对象反序列化回来
        FileInputStream fis = new FileInputStream("person.obj");
        ObjectInputStream ois = new ObjectInputStream(fis);
        /*
        Object readObject()
        该方法会进行对象的反序列化，如果对象流通过其连接的流读取的字节分析并非
        是一个java对象时，会抛出异常:ClassNotFoundException
        */
        Person p = (Person)ois.readObject();
        System.out.println(p);
    }
}