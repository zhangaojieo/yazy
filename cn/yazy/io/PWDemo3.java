package cn.yazy.io;
/*
PrintWriter的自动行刷新功能
如果实例化PW时第一个参数传入的是一个流，则此时可以再传入一个boolean型的参数，此值为true时
就打开了自动行刷新功能。
即:
每当我们用PW的println方法写出一行字符串后会自动flush.
 */

import java.io.*;
import java.util.Scanner;

/**
 * 自动行刷新功能
 * @author: ZhangAJ
 * @create: 2022年11月26日 15:50
 */
public class PWDemo3 {
    public static void main(String[] args) throws FileNotFoundException,
            UnsupportedEncodingException {
//打开自动行刷新
        PrintWriter pw = new PrintWriter(
                new BufferedWriter(
                        new OutputStreamWriter(
                                new FileOutputStream("pw3.txt"),
                                "UTF-8"
                        )
                ),true
        );
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入内容，单独输入exit停止");
        while(true){
            String line = scanner.nextLine();
            if("exit".equals(line)){
                break;
            }
            //pw.print(line);//不会flush
            pw.println(line);//自动flush
        }
        System.out.println("再见!");
        pw.close();
    }
}
