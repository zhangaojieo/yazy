package cn.yazy.io;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 转换字符输入流
 * 可以将读取的字节按照指定的字符集转换为字符
 *
 *
 * 转换流的意义:
 * 实际开发中我们还有功能更好用的字符高级流.但是其他的字符高级流都有一个共通点:不能直接连接在
 * 字节流上.而实际操作设备的流都是低级流同时也都是字节流.因此不能直接在流连接中串联起来.转换流
 * 是一对可以连接在字节流上的字符流,其他的高级字符流可以连接在转换流上.在流连接中起到"转换
 * 器"的作用(负责字符与字节的实际转换)
 * @author: ZhangAJ
 * @create: 2022年11月26日 15:46
 */
public class ISRDemo {
    public static void main(String[] args) throws IOException {
        //将osw.txt文件中的所有文字读取回来.
        FileInputStream fis = new FileInputStream("osw.txt");
        InputStreamReader isr = new InputStreamReader(fis,"UTF-8");
        /*
        字符流读一个字符的read方法定义:
        int read()
        读取一个字符,返回的int值实际上表示的是一个char(低16位有效).如果返回的
        int值表示的是-1则说明EOF
        */
        //测试读取文件中第一个字
        // int d = isr.read();
        // char c = (char)d;
        // System.out.println(c);
        //循环将文件所有字符读取回来
        int d;
        while((d = isr.read()) != -1){
            System.out.print((char)d);
        }
        isr.close();
    }
}