package cn.yazy.io;

import java.io.*;

/**
 * 使用缓冲流完成文件复制操作
 *
 * 缓冲流:
 * java.io.BufferedInputStream和BufferedOutputStream
 * 缓冲流是一对高级流，它们的作用是提高数据的读写效率
 *
 * 缓冲流内部维护一个8K字节数组，无论我们读写方式是什么(随机读写，块读写)，缓冲流
 * 都会同一转换成块读写来保证读写效率
 * @author: ZhangAJ
 * @create: 2022年11月26日 14:38
 */
public class CopyDemo3 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("D:/学习工具/CentOS-7-x86_64-DVD-2009.iso");
        BufferedInputStream bis = new BufferedInputStream(fis);
        FileOutputStream fos = new FileOutputStream("D:/学习工具/CentOS-7-x86_64-DVD-2009_cp2.iso");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        int d;
        long start = System.currentTimeMillis();
        while((d = bis.read())!=-1){
            bos.write(d);
        }
        long end = System.currentTimeMillis();
        System.out.println("复制完毕!耗时:"+(end-start)+"毫秒");
        bis.close();
        bos.close();
    }
}