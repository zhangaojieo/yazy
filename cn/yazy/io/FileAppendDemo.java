package cn.yazy.io;
/*
文件输出流-追加模式
    重载的构造方法可以将文件输出流创建为追加模式
    FileOutputStream(String path,boolean append)
    当第二个参数传入true时，文件流为追加模式，即:
    FileOutputStream(File file,boolean append)
    指定的文件若存在，则原有数据保留，新写入的数据
    会被顺序的追加到文件中
 */

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * 文件流的追加写模式
 * @author: ZhangAJ
 * @create: 2022年11月26日 14:28
 */
public class FileAppendDemo {
    public static void main(String[] args) throws IOException, IOException {
/*
FileOutputStream默认创建方式为覆盖模式，即:如果连接的文件存在，
则会将该文件原有数据全部删除。然后将通过当前流写出的内容保存到文件中。
重载的构造方法允许我们再传入一个boolean型参数，如果这个值为true，则
文件流为追加模式，即:若连接文件时该文件存在，原有数据全部保留，通过当前
流写出的数据会顺序的追加到文件中。
*/
        FileOutputStream fos = new FileOutputStream(
                "demo.txt", true);
        fos.write("热爱105°的你，".getBytes("UTF-8"));
        fos.write("滴滴清纯的蒸馏水。".getBytes("UTF-8"));
        System.out.println("写出完毕!");
        fos.close();
    }
}