package cn.yazy.io;

import java.io.FileInputStream;
import java.io.IOException;

/*
  使用文件输入流读取字节
 * @author: ZhangAJ
 * @create: 2022年11月21日 15:46
 */
public class FISDemo {
    public static void main(String[] args)
            throws IOException {
        /*
        demo.dat文件中的内容：
                00000001  00000010
         */
        FileInputStream fis =
                new FileInputStream("./demo.dat");
        /*
            public int read() throws IOException
            从文件中读取1个字节，并以int类型返回，而读取到这个字节的8位
            2进制数据会放在该int值得“低八位”上，高24位2进制全部补0
            当读到文件末尾时，返回的int值是一个特殊的值 “-1”

            第一次读取的字节：00000001
            返回的int值d 2进制的样子：
            00000000 00000000 00000000 00000001
         */
        int d = fis.read();
        System.out.println(d);//1
        /*
             第二次读取的字节：00000010
            返回的int值d 2进制的样子：
            00000000 00000000 00000000 00000010
         */
        d = fis.read();
        System.out.println(d);//2

         /*
             第二次读取到文件末尾
            返回的int值d 2进制的样子：
            11111111 11111111 11111111 11111111
         */
        d = fis.read();
        System.out.println(d);//-1

        fis.close();//关闭流资源

    }

}