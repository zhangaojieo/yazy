package cn.yazy.file_文件类;

import java.io.File;

/*
    删除一个目录
 * @author: ZhangAJ
 * @create: 2022年11月17日 8:53
 */
public class DeleteDirDemo {
    public static void main(String[] args) {
        //将当前目录下的demo目录删除
        //File dir = new File("demo");
        File dir = new File("a");
        if(dir.exists()){
            dir.delete();//delete方法删除目录时只能删除空目录
            System.out.println("目录已经删除！");
        }else{
            System.out.println("目录不存在！");
        }
    }
}