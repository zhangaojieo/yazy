package cn.yazy.file_文件类;

import java.io.File;
/*
    使用File创建目录
 * @author: ZhangAJ
 * @create: 2022年11月17日 8:47
 */
public class MkdirDemo {
    public static void main(String[] args) {
        //在当前目录下新建一个目录:demo
        //File dir = new File("demo");
        File dir = new File("./a/b/c/d/e/f");
        if (dir.exists()){
            System.out.println("该目录已经存在！");
        }else{
            //dir.mkdir();//创建目录时所在的目录必须已经存在
            dir.mkdirs();//创建目录时会将路径上所有不存在的目录一起创建
            System.out.println("目录已创建！");
        }
    }
}
