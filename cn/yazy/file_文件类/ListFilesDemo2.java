package cn.yazy.file_文件类;

import java.io.File;
import java.io.FileFilter;

/*
获取目录中符合特定条件的子项
重载的listFiles方法:File[] listFiles(FileFilter)
该方法要求传入一个文件过滤器，并仅将满足该过滤器要求的子项返回。
 * @author: ZhangAJ
 * @create: 2022年11月21日 14:20
 */
public class ListFilesDemo2 {
    public static void main(String[] args) {
        //获取当前目录中的所有名字以.开头的
        File dir = new File(".");
        if (dir.isDirectory()) {
//            FileFilter filter = new FileFilter() {//匿名内部类创建过滤器
//                @Override
//                public boolean accept(File file) {
//                    String name = file.getName();//获取文件夹下的文件名
//                    //判断标准，以“.”开头
//                    boolean start = name.startsWith(".");
//                    System.out.println("过滤器过滤：" + name + "是符合要求" + start);
//                    return start;
//                }
//            };
//            File[] subs = dir.listFiles(filter);//方法内部会调用accept方法

            File[] subs = dir.listFiles(new FileFilter() { // 通过匿名内部类创建过滤器
                @Override
                public boolean accept(File file) {
                    //名字是否以 ”.“开头
                    return file.getName().startsWith(".");
                }
            });
            System.out.println(subs.length);
        }
    }
}