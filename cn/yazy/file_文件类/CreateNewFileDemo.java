package cn.yazy.file_文件类;

import java.io.File;
import java.io.IOException;

/*
    使用File创建一个文件
 * @author: ZhangAJ
 * @create: 2022年11月17日 8:38
 */
public class CreateNewFileDemo {
    public static void main(String[] args) throws IOException {
        //在当前目录下新建一个文件
        File file = new File("./test.txt");
        // boolean exists() 判断当前File表示的位置是否已经实际存在
        //该文件或目录
        if(file.exists()){
            System.out.println("该文件已经存在！");
        }else{
            file.createNewFile();
            System.out.println("文件已经创建！");
        }
    }
}