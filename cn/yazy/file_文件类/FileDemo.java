package cn.yazy.file_文件类;


import java.io.File;

/*
    File类
  File类的每一个实例可以表示硬盘(文件系统)中的一个
文件或目录(实际上表示的是一个抽象路径)
使用File可以做到:
    1:访问其表示的文件或目录的属性信息,例如:名字,大小,修改时间等等
    2:创建和删除文件或目录
    3:访问一个目录中的子项,但是File不能访问文件数据.
 * @author: ZhangAJ
 * @create: 2022年11月17日 8:27
 */
public class FileDemo {
    public static void main(String[] args) {
        //使用File访问当前项目目录下的demo.txt文件
       /*
        创建File时要指定路径，而路径通常使用相对路径。
        相对路径的好处在于有良好的跨平台性。
        "./"是相对路径中使用最多的，表示"当前目录"，而当前目录是哪里
        取决于程序运行环境而定，在idea中运行java程序时，这里指定的
        当前目录就是当前程序所在的项目目录。
        */
        //File file = new File("c:/xxx/xx/xx/xx/xx.txt");
        File file = new File("./demo.txt");
        //获取名字
        String fileName = file.getName();
        System.out.println(fileName);
        //获取文件的大小
        long length = file.length();
        System.out.println(length+"字节");
        //是否可读可写
        boolean cr = file.canRead();
        boolean cw = file.canWrite();
        System.out.println("是否可读："+cr);
        System.out.println("是否可写："+cw);
        //是否隐藏
        boolean hidden = file.isHidden();
        System.out.println("是否隐藏："+hidden);
    }
}