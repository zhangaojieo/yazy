package cn.yazy.file_文件类;

import java.io.File;

/*
    使用File删除一个文件
 * @author: ZhangAJ
 * @create: 2022年11月17日 8:43
 */
public class DeleteFileDemo {
    public static void main(String[] args) {
        //将当前目录下的test.txt文件删除
        /*
            相对路径中的“./”可以忽略不写，默认就是从当前目录开始的
         */
        File file = new File("test.txt");
        if(file.exists()){
            file.delete();
            System.out.println("文件已经删除！");
        }else{
            System.out.println("文件不存在！");
        }
    }
}