package cn.yazy.file_文件类;
import java.io.File;
/*
  访问一个目录中的所有子项
 * @author: ZhangAJ
 * @create: 2022年11月17日 9:21
 */
public class ListFilesDemo {
    public static void main(String[] args) {
        //获取当前目录中的所有子项
        File dir = new File(".");
        /*
            boolean isDirectory()
            判断当前File表示的是否为一个目录
            boolean isFile()
            判断当前File表示的是否为一个文件
         */
        if(dir.isDirectory()){
            /*
                File[] listFiles()
                将当前目录中所有子项返回，返回得的数组中每个
                File实例表示其中一个子项
             */
            File[] files = dir.listFiles();
            System.out.println("当前目录包含："+files.length+"个子项");
            for (int i=0;i<files.length;i++) {
                File file = files[i];
                System.out.println(file.getName());
            }
        }
    }
}
