package cn.yazy.file_文件类;

import java.io.File;
import java.io.FileFilter;
/*
Lambda表达式
    JDK8之后,java支持了lambda表达式这个特性.
    lambda可以用更精简的代码创建匿名内部类.但是该匿名内部类实现的接口只能有一个抽象方法,
 否则无法使用!
    lambda表达式是编译器认可的,最终会将其改为内部类编译到class文件中
    语法:
     (参数列表)->{
        方法体
     }
 * @author: ZhangAJ
 * @create: 2022年11月21日 14:40
 */
public class LambdaDemo {
    public static void main(String[] args) {
        //匿名内部类创建FileFilter
        FileFilter filter = new FileFilter(){
            @Override
            public boolean accept(File f) {
                return f.getName().startsWith(".");
            }
        };

        FileFilter filter1 = (File file)->{
            return  file.getName().startsWith(".");
        };

        //lambda表达式中参数的类型可以忽略不写
        FileFilter filter2 =(file)->{
            return  file.getName().startsWith(".");
        };
        //lambda表达式方法体中若只有一句代码，则{}可以省略不写
        //如果这句话有return关键字，那么return也要一并省略
        FileFilter filter3 =(file)->file.getName().startsWith(".");

    }
}