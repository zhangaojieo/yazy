package cn.yazy.集合.map;

import java.util.HashMap;
import java.util.Map;

/*
  Map概述:
  在现实生活中，我们常会看到这样的一种集合：IP地址与主机名，身份证号与个人等，这种一 一
对应的关系，就叫做映射。
  Map<K,V>是 Java 中的映射接口。K-Key---健，V-Value---值---存储的时候是一个键对应一个
值---键值对---要求键必须唯一，值随意，一个 Map 中存储了很多的键值对。
Java提供了专门的集合类用来存放这种关系的对象，即java.util.Map 接口。
特点:
    1.Map中toString(); 方法也做了重写。
    2.Map中不保证元素的存入顺序。
    3.Map中如果出现了重复的键，则对应的值覆盖，键不变。
    4.Map.Entry<K,V>代表键值对的接口，每一个Entry对象代表一个键值对。一个Map对象实际
    上由多个Entry对象组成。是一个内部接口。
Map常用子类:
       通过查看Map接口描述，看到Map有多个子类。
       HashMap：存储数据采用的哈希表结构，允许键或者值为null。默认的初始容量是16，加载因
    子是0.75f，每次扩容一倍，是一个异步式线程不安全的映射元素的存取顺序不能保证一致。由
    于要保证键的唯一、不重复，需要重写键的hashCode()方法、equals()方法。
       LinkedHashMap：HashMap下有个子类LinkedHashMap，存储数据采用的哈希表结构+链表
    结构。通过链表结构可以保证元素的存取顺序一致；通过哈希表结构可以保证的键的唯一、不重
    复，需要重写键的hashCode()方法、equals()方法。
       Hashtable：不允许键或者值为 null。默认的初始容量是 11，加载因子是 0.75f。是一个同步
    式线程安全的映射。
       ConcurrentHashMap：异步式线程安全的映射。
    常用方法:
        public V put(K key, V value) : 把指定的键与指定的值添加到Map集合中。
        public V remove(Object key) : 把指定的键 所对应的键值对元素 在Map集合中删除，
        返回被删除元素的值。
        public V get(Object key):根据指定的键，在Map集合中获取对应的值。
        public Set<K> keySet() : 获取Map集合中所有的键，存储到Set集合中。
        public Set<Map.Entry<K,V>> entrySet() : 获取到Map集合中所有的键值对对象的集
        合(Set集合)
 * @author: ZhangAJ
 * @create: 2022年11月14日 14:33
 */
public class MapDemo1 {
    public static void main(String[] args) {
        //创建Map集合
        Map<String,Integer> map = new HashMap<>();
        /*
           V put(K key, V value);
           将给定的键值对存入Map
           Map有一个要求，即：Key值不允许重复（Key的equals比较）
           因此如果使用重复的key存入value,则是替换value操作，此时
           put方法的返回值就是被替换的value,否则返回值为null。
         */
        /*
            注意，如果value的类型是包装类，切记不要用基本类型接收返回值，
            避免因为自动拆箱导致的空指针。
         */
        Integer value =  map.put("Java",90);
        // 第一次使用 返回值是空
        System.out.println(value);//null
        map.put("数学",91);
        map.put("英语",94);
        map.put("C++",93);
        map.put("Python",98);
        System.out.println(map);//{Java=90, C++=93, 数学=91, 英语=94, Python=98}
        // 第二次使用 如果key的值相同  则替换值 并返回
        value =  map.put("Java",91);
        System.out.println(value);//90

        /*
             V get(Object key)
             根据给定得的key值获取对应的value,若给定得的key
             不存在则返回值为null
         */
        Integer javaScore =  map.get("Java");
        System.out.println(javaScore);//91

        Integer pythonScore = map.get("Python");
        System.out.println(pythonScore);//98

        /*
            V remove(Object key)
            删除给定的key所对应的键值对，返回值为这个key对应的
            value
         */
        Integer  removeValue = map.remove("英语");
        System.out.println(removeValue);//94

        /*
             int size()
             获取集合的长度，返回值为集合的长度
         */
        int size = map.size();
        System.out.println(size);//4

        /*
            boolean containsKey(Object key)
            判断当前Map是否包含指定的key
            boolean containsValue(Object value)
            判断当前Map是否包含指定的value
         */
        boolean containsKey =  map.containsKey("Java");
        System.out.println("是否包含key:" + containsKey);//true
        boolean containsValue =  map.containsValue(100);
        System.out.println("是否包含value:" + containsValue);//false


        /*
            课堂作业：
                利用Map集合将上学期所有的考试成绩
             进行添加，要求：
                1.单独获取Java基础和Mysql的考试成绩
                2.获取上学期课程的总门数
                3.删除Java基础以及Mysql的考试成绩
                4.查看剩余的成绩
                5.判断集合中是否有100分的课程
                6.判断集合中是否有体育课的成绩
         */



    }

}