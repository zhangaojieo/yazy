package cn.yazy.集合.map;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*
  Map集合的遍历
    map集合提供了3种遍历方式
        1.遍历所有的key
        2.遍历每一组键值对
        3.遍历所有的value(不常用)
 * @author: ZhangAJ
 * @create: 2022年11月14日 14:33
 */
public class MapDemo2 {
    public static void main(String[] args) {

        Map<String, Integer> map =
                new HashMap<>();
        map.put("java基础",80);
        map.put("大学英语",78);
        map.put("大学语文",89);
        map.put("高等数学",76);
        map.put("Mysql",66);
        System.out.println(map);//

        System.out.println("======================");

        /*
            1、遍历所有的key
            Set<K> keySet();
            将当前Map中所有的key以一个Set集合的形式返回
            遍历该Set集合就等同于遍历了所有的key
         */
        Set<String> keySet = map.keySet();
        //遍历Set集合就等同于遍历Map集合的所有key
        for (String str: keySet) {
            System.out.println("当前集合的所有key:"+str);
        }

        System.out.println("==========================");

        /*
          2.遍历所有的value
          Collection<V> values();
          将当前的map集合中的所有value以一个集合的形式返回
         */
        Collection<Integer> values = map.values();
        for (Integer value : values) {
            System.out.println("当前集合的所有value:"+value);
        }

        System.out.println("==========================");
     /*
        集合在使用foreach遍历时，并不要求过程中不能通过集合
        的方法增删元素，而之前的迭代器遍历则有此要求，否则可能
        在遍历过程中抛出异常
     */
        values.forEach(i-> System.out.println("value:"+i));

        System.out.println("==========================");

        /*
           3. 遍历每一组键值对
           Set<Map.Entry<K, V>> entrySet();
           将当前Map中每一组键值对以一个Entry实例形式存入Set集合后返回
           java.util.Map.Entry
           Entry的每一个实例用于表示Map中的一组键值对
           其中有两个常用方法：
            K getKey(); //获取Map集合的key
            V getValue();//获取Map集合的value
         */
        //将Map集合的键值对转为Entry实例存入Set集合
        Set<Map.Entry<String, Integer>> entrySet = map.entrySet();
        //遍历Set集合中的每一个Entry从而取出键和值
        for (Map.Entry<String, Integer> entry : entrySet) {
            String key = entry.getKey();
            Integer value =  entry.getValue();
            System.out.println(key + " " + value);
        }

        System.out.println("==========================");
        /*
            java8集合框架支持lambda表达式遍历
            因此Map集合和Collection集合都提供foreach方法，
            通过lambda表达式遍历元素
         */
        map.forEach(
                (k,v)-> System.out.println(k+" "+v));




    }


}