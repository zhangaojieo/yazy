package cn.yazy.集合.map;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/*
  LinkedHashMap概述：
    Map接口的哈希表和链接列表实现，具有可
    预知迭代顺序。
    特点：
        底层的数据结构是链表和哈希表，元素有序并且唯一
        元素的有序性由链表数据结构保证，唯一性由哈希表数据结构保证。
    Map集合的数据结构只和键有关
 * @author: ZhangAJ
 * @create: 2022年11月14日 15:18
 */
public class MapDemo3 {
    public static void main(String[] args) {

        LinkedHashMap<String, String> map =
                new LinkedHashMap<>();

        map.put("部门","项目部");
        map.put("编号","pm001");
        map.put("姓名","张三");

//        map.put("部门","项目部");
//        map.put("编号","pm002");
//        map.put("姓名","李四");

        //将Map集合的键值对转为Entry实例存入Set集合
        Set<Map.Entry<String, String>> entrySet = map.entrySet();
        //遍历Set集合中的每一个Entry从而取出键和值
        for (Map.Entry<String, String> entry : entrySet) {
            String key = entry.getKey();
            String value =  entry.getValue();
            System.out.println(key + " " + value);
        }

    }
}