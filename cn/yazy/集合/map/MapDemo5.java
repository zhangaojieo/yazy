package cn.yazy.集合.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * @author: ZhangAJ
 * @create: 2022年11月14日 19:36
 */
public class MapDemo5 {
    public static void main(String[] args) {

        /*
           集合嵌套
           ArrayList嵌套HashMap
         */
        HashMap<String,String> map =  new HashMap<>();
        map.put("吕布","貂蝉");
        map.put("周瑜","小乔");
        HashMap<String,String> map2 =  new HashMap<>();
        map2.put("郭靖","黄蓉");
        map2.put("杨过","小龙女");
        HashMap<String,String> map3 =  new HashMap<>();
        map3.put("令狐冲","任盈盈");
        map3.put("林平之","岳灵珊");

        ArrayList<HashMap<String,String>> list =
                new ArrayList<>();
        list.add(map);
        list.add(map2);
        list.add(map3);
        //遍历嵌套循环
        //先遍历 list集合  拿到每个HashMap
        for (HashMap<String,String> hashMap : list){
            //再将HashMap 转为Set
            Set<Map.Entry<String, String>> entrySet =
                    hashMap.entrySet();
            //再去遍历Set集合
            for(Map.Entry<String,String> entry : entrySet){
                //根据Map的Entry 的方法获取对应的key和value
                System.out.println(
                        entry.getKey()+ " " + entry.getValue());
            }
        }
    }
}