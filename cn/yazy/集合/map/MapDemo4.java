package cn.yazy.集合.map;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/*
  TreeMap集合
    键的数据结构是红黑树，可保证键得的排序和唯一性
    排序分为自然排序和比较器排序
    线程不是安全的，效率比较高
    TreeMap集合排序，实现Comparable接口
        重写compareTo方法，使用比较器
 * @author: ZhangAJ
 * @create: 2022年11月14日 15:28
 */
public class MapDemo4 {
    public static void main(String[] args) {

        //自然排序
        TreeMap<Integer, Integer> treeMap =
                new TreeMap<>();
        treeMap.put(02,80);
        treeMap.put(03,78);
        treeMap.put(01,89);
        treeMap.put(04,76);
        treeMap.put(05,66);
        System.out.println(treeMap);

        System.out.println("====================");


        TreeMap<Phone, String> phoneMap =
                new TreeMap<>();
        phoneMap.put(
                new Phone("OPPO",2000),"中国");
        phoneMap.put(
                new Phone("MI",8500),"中国");
        phoneMap.put(
                new Phone("iPhone",15000),"美国");
        phoneMap.put(
                new Phone("HUAWEI",6000),"中国");
        System.out.println(phoneMap);

        //将Map集合的键值对转为Entry实例存入Set集合
        Set<Phone> keySet = phoneMap.keySet();
        //遍历Set集合中的每一个Entry从而取出键和值
        for (Phone phone : keySet) {
            System.out.println(phone.getPrice());
        }
    }
}
class Phone implements Comparable<Phone>{
    private String name;
    private Integer price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Phone() {
    }

    public Phone(String name, Integer price) {

        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if(this == o)
            return  true;
        if(this == null)
            return  false;
        if(getClass()!=o.getClass())
            return false;
        Phone other = (Phone)o;
        if (name==null){
            if (other.name != null)
                return false;
        }else if (!name.equals(other.name)){
            return false;
        }
        if (price==null){
            if (other.price!=null)
                return  false;
        }else if(!price.equals(other.price)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final  int prime = 31;
        int result =1;
        result = prime * result+((name==null)?0:name.hashCode());
        result = prime * result +((price==null)?0:price.hashCode());
        return  result;
    }

    //按照价格比较排序
    @Override
    public int compareTo(Phone o) {
        return this.getPrice() -o.getPrice();
    }
}