package cn.yazy.集合.queue;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 *Queue 集合
 *概述：java.util.Queue是Collection接口下子接口，底层是队列的数据结构存储，遵循先进
 *先出的原则，先放入的元素叫队头元素，最后放入的叫队尾元素。
 *常用方法:
 *    boolean offer(E e)插入元素，成功true失败false
 *    E peek()获取队头元素，失败返回null
 *    E element()获取队头元素，失败则抛出java.util.NoSuchElementException(没有元素异常)
 *    E poll()出队移除队头元素，失败返回null
 *    E remove()移除队头元素，失败则抛出java.util.NoSuchElementException(没有元素异常)
 *    boolean add(E e)在队列末尾追加元素，成功true失败false。
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年11月7日下午3:19:03
 */
public class QueueDemo {
    public static void main(String[] args) {
        //创建队列
        Queue<String> q=new LinkedList<String>();
        //插入元素
        q.offer("a");
        q.offer("b");
        q.offer("c");
        q.offer("d");
        q.add("e");//追加元素
        System.out.println(q);//[a, b, c, d, e]
        //获取队头元素 失败返回null
        System.out.println(q.peek());//a
        //获取队头元素 失败 报java.util.NoSuchElementException（没有元素异常）
        System.out.println(q.element());//a
        //出队(移除队头元素)
        System.out.println(q.poll());//a
        System.out.println(q);//[b, c, d, e]
        //移除队头元素，失败则抛出java.util.NoSuchElementException(没有元素异常)
        System.out.println(q.remove());//b
        System.out.println(q);//[c, d, e]
    }
}