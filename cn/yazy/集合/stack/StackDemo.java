package cn.yazy.集合.stack;

import java.util.Stack;

/**
 * Stack(栈)概述:
 *Stack是List集合的实现类，并且继承了Vector，遵循先进后出（LIFO）的原则。
 *  栈顶元素---最后放入栈中的元素。
 * 栈底元素---最先放入栈中的元素。
 * 入栈/压栈---将元素放入栈中。
 * 出栈/弹栈---将元素从栈中取出。
 *常用方法:
 *  public E push(E item)将指定元素item压入当前栈中
 *  public synchronized E peek()获取栈顶元素(不删除)
 *注意:如果栈为空则报java.util.EmptyStackException(空栈异常)
 * public synchronized E pop()出栈/弹栈，获取并删除栈顶元素
 *  注意:如果栈为空则报java.util.EmptyStackException(空栈异常)
 * public synchronized int search(Object o)
 * 获取指定元素o在栈中的位置，获取时从栈顶到栈底的顺序查找，以1为基数，若没有返回-1。
 *@author ZhangAJ:
 *@version 创建时间：2022年11月7日下午2:54:20
 */
public class StackDemo {
    public static void main(String[] args) {
        //创建Stack
        Stack<String> s =  new Stack<String>();
        //压栈
        s.push("a");
        s.push("b");
        s.push("c");
        s.push("d");
        //获取栈顶元素（不删除） 如果没有报java.util.EmptyStackException异常
        System.out.println(s.peek());// d
        //获取栈顶元素（删除）如果没有报java.util.EmptyStackException异常
        System.out.println(s.pop());// d
        System.out.println(s); //[a, b, c]
        //获取指定元素的位置
        System.out.println(s.search("a"));//3
    }
}