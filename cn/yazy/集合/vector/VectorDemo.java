package cn.yazy.集合.vector;

import java.util.Vector;

/**
 * Vector类:
 *是Java中的最早的集合，基于数组。默认初始容量为10，
 *默认扩容每次增加一倍，基于了三元运算，是一个线程安全的集合。
 *@author ZhangAJ:
 *@version 创建时间：2022年11月7日下午2:47:28
 */
public class VectorDemo {

    public static void main(String[] args) {
        //创建一个Vector类
        //Vector<String> v =
        //new Vector<String>(10,5);
        Vector<String> v =  new Vector<String>();
        for(int i=0;i<11;i++){
            v.add("a");
        }
        System.out.println(v.size());//获取实际元素的数量
        System.out.println(v.capacity());//获取集合容量 20
    }
}