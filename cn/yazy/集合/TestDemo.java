package cn.yazy.集合;
/**
 *@author ZhangAJ:
 *@version 创建时间：2022年10月20日上午8:26:34
 */
public class TestDemo {

    public static void main(String[] args) {

        int[] a = {12,34,45,66,7};
        //获取数组的长度
        System.out.println(a.length);

        String str = "我是一个字符串";
        //获取字符串长度
        System.out.println(str.length());
    }

}
