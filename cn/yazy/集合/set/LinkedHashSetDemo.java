package cn.yazy.集合.set;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author: ZhangAJ
 * @create: 2022年11月10日 10:56
 */
public class LinkedHashSetDemo {
    public static void main(String[] args) {
        Set<String> set = new LinkedHashSet<String>();
        set.add("bbb");
        set.add("aaa");
        set.add("ccc");
        set.add("ddd");
        Iterator it = set.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

    }

}

