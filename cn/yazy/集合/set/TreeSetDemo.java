package cn.yazy.集合.set;
import java.util.TreeSet;
/*
 *TreeSet集合:
 *TreeSet是SortedSet接口的实现类，正如SortedSet名字所暗示的，TreeSet可以确保集合
 *元素处于排序状态。与HashSet集合相比，TreeSet还提供了如下几个额外的方法。
 *TreeSet常用方法:
 *    Comparator comparator():如果TreeSet采用了定制排序，则该方法返回定制排序所使用
 *的Comparator;如果TreeSet采用了自然排序，则返回null。
 *    Object first():返回集合中的第一个元素。
 *    Object last():返回集合中的最后一个元素。
 *    Object lower(Object e):返回集合中位于指定元素之前的元素(即小于指定元素的最大元
 *素，参考元素不需要是TreeSet集合里的元素)。
 *   Object higher(Object e):返回集合中位于指定元素之后的元素(即大于指定元素的最小
 *元素，参考元素不需要是TreeSet集合里的元素)。
 *    SortedSet subSet(Object fromElement, Object toElement):返回此Set的子集合，
 *范围从fromElement(包含)到toElement(不包含)。
 *    SortedSet headSet(Object toElement):返回此Set的子集，由小于toElement的元素
 *组成。
 *    SortedSet tailSet(Object fromElement):返回此Set的子集，由大于或等于
 *fromElement的元素组成。
 *表面上看起来这些方法很多，其实它们很简单：因为TreeSet中的元素是有序的，所以增加了
 *访问第一个、前一个、后一个、最后一个元素的方法，并提供了三个从TreeSet中截取子TreeSet
 *的方法。
 * @author: ZhangAJ
 * @create: 2022年11月10日 8:40
 */


public class TreeSetDemo {

    public static void main(String[] args) {

        //创建TreeSet，并为其添加五个Integer对象
        TreeSet<Integer> nums = new TreeSet<>();

        nums.add(5);
        nums.add(3);
        nums.add(-2);
        nums.add(-9);
        nums.add(10);

        //输出集合中的所有元素，此时集合已经排序好了
        System.out.println(nums);//[-9, -2, 3, 5, 10]
        //输出集合的第一个元素
        System.out.println(nums.first());//-9
        //输出集合的最后一个元素
        System.out.println(nums.last());//10
        //输出指定元素之前的元素值（可以不是TreeSet集合中的值）
        System.out.println(nums.lower(2));//-2
        //输出指定元素之后的元素值 如果给定值为集合的最后一个元素 则返回null
        System.out.println(nums.higher(3));//5
        //输出集合的子集 需要传入两个值 开始 和 结束 包含开始值 不包含结束值
        System.out.println(nums.subSet(-2,5));//[-2, 3]
        //返回比指定元素小的子集
        System.out.println(nums.headSet(3));//[-9, -2]
        //返回此Set的子集，由大于或等于的元素值
        System.out.println(nums.tailSet(3));//[3, 5, 10]

        /*
        备注：与HashSet集合采用hash算法来决定元素的存储位置不同，TreeSet采用红黑树的数据结
        构来存储集合元素。那么TreeSet进行排序的规则有两种：自然排序和定制排序。
         */

    }

}