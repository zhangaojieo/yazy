package cn.yazy.集合.set;

import java.util.HashSet;
import java.util.Iterator;

/**
 *
 *Set概述：
 *   Set接口是Collection的子接口，只包含Collection中继承过来的方法，并增加了对add()方法的
 *限制，不允许有重复的元素。Set接口重写了hashCode()及equals()方法,允许Set实例进行内容
 *上的比较，即使实现类型不同，集合元素相同，那么它们就是相等的，Set的实现类为:HashSet、
 *TreeSet和LinkedHashSet
 *HashSet集合:
 *    HashSet集合，采用哈希表结构存储数据，保证元素唯一性的方式依赖于：hashCode()与equals()
 *方法, 哈希表底层使用的也是数组机制，数组中也存放对象，而这些对象往数组中存放时的位置
 *比较特殊，当需要把这些对象给数组中存放时，那么会根据这些对象的特有数据结合相应的算法，
 *计算出这个对象在数组中的位置，然后把这个对象存放在数组中。而这样的数组就称为哈希数组，
 *即就是哈希表。
 *  总结：保证HashSet集合元素的唯一，其实就是根据对象的hashCode和equals方法来决定的。
 *如果我们往集合中存放自定义的对象，那么保证其唯一，就必须复写hashCode和equals方法建
 *立属于当前对象的比较方式
 *@author ZhangAJ:
 *@version 创建时间：2022年11月7日下午3:32:45
 */
public class HashSetDemo1 {
    public static void main(String[] args) {
        // 创建 HashSet 对象
        HashSet<String> hs = new HashSet<String>();
        // 给集合中添加自定义对象
        hs.add("张三");
        hs.add("李四");
        hs.add("王五");
        hs.add("张三");
        // 取出集合中的每个元素
        Iterator<String> it = hs.iterator();
        while (it.hasNext()) {
            String s = it.next();
            System.out.println(s);
        }
    }

}