package cn.yazy.集合;

import java.util.LinkedHashSet;
import java.util.Scanner;

/**
 * 1.使用Scanner从键盘读取一行输入,去掉其中重复的字符,打印出那些不同的字符
 *
 * 2.创建一个List集合存储若干个重复元素,自己定义方法去除重复的元素,遍历一下去重后的List集合
 *
 * 3.定义一个泛型为String类型的集合,统计该集合中每个字符出现的次数,(注意不是字符串)例如集合中有
 *   "abc""bcd"俩个元素,程序最终输出的结果为"a=1,b=2,c=2,d=1"
 *@author ZhangAJ:
 *@version 创建时间：2022年10月26日上午9:50:33
 */
public class TestDemo3 {
    public static void main(String[] args) {

        //用hashset接收
        LinkedHashSet<Character> a =new LinkedHashSet<>();

        //键盘录入
        Scanner sc =new Scanner(System.in);
        String s =sc.next();

        //字符串转换成字符数组 获取每一个字符存储在hashset中  自动去除重复
        char [] arr = s.toCharArray();

        //遍历字符数组
        for (char c : arr) {
            a.add(c);
        }

        //遍历hashset 打印每一个字符
        for (Character  c: a) {
            System.out.println(c);

        }
    }
}