package cn.yazy.集合.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 迭代器遍历过程中不得通过集合的方法增删元素
 *@author ZhangAJ:
 *@version 创建时间：2022年10月27日上午8:27:06
 */
public class CollectionDemo6 {

    public static void main(String[] args) {

        Collection coll1 = new ArrayList();
        coll1.add("JAVA");
        coll1.add("#");
        coll1.add("C++");
        coll1.add("#");
        coll1.add("C语言");
        coll1.add("#");
        coll1.add("Python");
        coll1.add("#");
        coll1.add("ios");
        System.out.println(coll1);

        //获取迭代器
        Iterator it = coll1.iterator();
        while(it.hasNext()) {
            String str =(String)it.next();
            System.out.println(str);
            if("#".equals(str)) {
                /*
                 *  迭代器遍历过程中不得通过集合的方法增删元素
                 * 否则抛出异常：
                 * 		java.util.ConcurrentModificationException
                 */
                //coll1.remove(str);
                /*
                 * 迭代器的remove方法可以通过next方法获取的元素从集合
                 * 中删除
                 */
                it.remove();

            }
        }

        System.out.println(coll1);

    }

}