package cn.yazy.集合.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 * 集合的遍历
 * Collection提供了统一的遍历集合方式,迭代器模式
 *
 * Iterator iterator()
 * 该方法会获取一个用于遍历当前集合元素的迭代器
 *
 * java.util.Iterator接口
 * 迭代器接口,定义了迭代器遍历集合的相关操作
 *
 * 不同的集合都实现了一个用于遍历自身元素的迭代器实现类,我们无需记住它们的名字,
 * 用多态的角度吧它们看做为Iterator即可
 *
 * 迭代器遍历集合遵循步骤为:问 取 删,其中删除元素不是必要操作
 *@author ZhangAJ:
 *@version 创建时间：2022年10月24日下午2:38:23
 */
public class CollectionDemo5 {

    public static void main(String[] args) {

        Collection coll1 = new ArrayList();
        coll1.add("JAVA");
        coll1.add("C++");
        coll1.add("C语言");
        coll1.add("Python");
        coll1.add("ios");
        System.out.println(coll1);//[JAVA, C++, C语言, Python, ios]


        //获取迭代器
        Iterator it = coll1.iterator();
        /*
         * 迭代器提供的相关方法
         * boolean hasNext();判断集合是否还有元素可以遍历
         *
         * E next();//获取集合的下一个元素
         * (第一个调用对象时就是第一个元素,以此类推)
         */
        while(it.hasNext()) {
            String str = (String)it.next();
            System.out.print(str);//JAVAC++C语言Pythonios
        }
    }
}
