package cn.yazy.集合.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 增强for循环
 * JDK5之后推出一个新特性:增强型for循环
 * 也称为新循环,使得我们可以使用相同的语法遍历集合或数组
 *
 * 语法:
 *     for(元素类型 变量名:集合或数组){
 *         循环体
 *     }
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年10月27日上午8:36:41
 */
public class CollectionDemo7 {

    public static void main(String[] args) {

        String[] str = {"唐僧","孙悟空","猪悟能","沙悟净","白龙马"};
        //传统for循环遍历
        for(int i=0;i<str.length;i++) {
            System.out.println(str[i]);
        }
        System.out.println("====================");

        //新for循环遍历数组
        for(String arr : str) {
            System.out.println(arr);
        }
        System.out.println("====================");
        //创建集合
        Collection coll1 = new ArrayList();
        coll1.add("JAVA");
        coll1.add("#");
        coll1.add("C++");
        coll1.add("#");
        coll1.add("C语言");
        coll1.add("#");
        coll1.add("Python");
        coll1.add("#");
        coll1.add("ios");
        coll1.add(123);
        //获取迭代器
        Iterator it = coll1.iterator();
        //迭代器遍历
        while(it.hasNext()) {
            String s =(String)it.next();
            System.out.println(s);
        }
        System.out.println("====================");
        //新for循环遍历集合
        for (Object object : coll1) {
            String ss = (String)object;
            System.out.println(ss);
        }

        //注意： 增强for  输入 fore 按alt + /
        //回车后就会自动遍历靠近当前循环的集合或数组

    }
}