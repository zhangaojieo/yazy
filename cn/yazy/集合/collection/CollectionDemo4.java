package cn.yazy.集合.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * 集合间的操作
 * 集合提供了如取并集、删交集、判断包含子集等操作
 *@author ZhangAJ:
 *@version 创建时间：2022年10月24日下午2:15:21
 */
public class CollectionDemo4 {

    public static void main(String[] args) {
        //Collection coll1 = new ArrayList();
        Collection coll1 = new HashSet();//不允许出现重复值
        coll1.add("JAVA");
        coll1.add("C++");
        coll1.add("C语言");
        coll1.add("Python");
        System.out.println(coll1);//[JAVA, C++, C语言, Python]

        Collection coll2 = new ArrayList();
        coll2.add("JAVA");
        coll2.add("android");
        coll2.add("ios");
        System.out.println(coll2);//[JAVA, android, ios]

        /*
         * 将给定集合中的元素添加到当前集合中，当前集合若发生了改变则返回true
         *  boolean addAll(Collection c);
         */
        boolean b1 = coll1.addAll(coll2);//取并集
        System.out.println(b1);//true
        System.out.println(coll1);//[JAVA, C++, C语言, Python, JAVA, android, ios]
        System.out.println(coll2);//[JAVA, android, ios]

        Collection coll3 = new ArrayList();
        coll3.add("JAVA");
        coll3.add("C++");
        coll3.add("Python");
        System.out.println(coll3);//[JAVA, C++, Python]

        /*
         * 判断当前集合是否包含给定集合中的所有元素
         * boolean containsAll(Collection c);
         */
        boolean b2 = coll1.containsAll(coll3);
        System.out.println(b2);//true

        /*
         * 删除当前集合中与给定集合中的共有元素
         * boolean removeAll(Collection c);
         */
        coll1.removeAll(coll3);
        System.out.println(coll1);//[C语言, android, ios]
        System.out.println(coll3);//[JAVA, C++, Python]

    }

}