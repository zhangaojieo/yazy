package cn.yazy.集合.collection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 集合
 * 集合与数组一样,可以保存一组元素,并且提供了操作元素的相关方法,使用更加方便
 *
 * Java中集合相关的接口
 *    java.util.Collection
 *       Collection集合是所有集合得到顶级接口,它下面有多种实现类,因此我们有着更多的数据结构可以选择
 *       Collection下常见的子接口:
 *           java.util.List:接口,线性表,是可重复集合,并且有序
 *               java.util.ArrayList
 *               java.util.LinkedList
 *           java.util.set:不可重复集合,大部分实现类是无序的
 *               java.util.Hashset
 * 这里所说的可重复是指集合中的元素是否可重复,而判定重复元素的标准是依靠元素自身的equals比较的结果
 *    如果为true就认为是重复元素
 *@author ZhangAJ:
 *@version 创建时间：2022年10月20日上午8:36:47
 */
public class CollectionDemo1 {

    public static void main(String[] args) {

        //创建集合
        Collection coll = new ArrayList();


        /*
         * 1.为集合添加元素 当元素添加成功后返回true
         *   boolean add(E e);
         */
        coll.add("张三");
        coll.add("男");
        coll.add(18);
        System.out.println(coll);

        /*
         * 2.返回当前集合元素的个数
         *   int size();
         */
        int size = coll.size();
        System.out.println("当前集合元素的个数:" + size);//3

        /*
         * 3.判断当前集合是否为空(不含有任何元素)
         *   boolean isEmpty();
         * 返回值为false 代表集合不为空
         * 放回置为true  代表集合为空
         */

        boolean isEmpty = coll.isEmpty();
        System.out.println("是否为空集:" + isEmpty);//false

        /*
         * 4.清空当前集合中的所有元素
         * void clear();
         */
        coll.clear();
        System.out.println("当前集合元素的个数:" + coll.size());//0
        System.out.println(
                "是否为空集：" + coll.isEmpty());//true
    }

}