package cn.yazy.集合.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;

/**
 *@author ZhangAJ:
 *@version 创建时间：2022年10月27日上午9:37:52
 */
public class TestDemo4 {

    public static void main(String[] args) {
        ArrayList<Integer> nList = new ArrayList<>(Arrays.asList(1,3,6,3,6,8,4,9,5,2,0));
        System.out.println(nList);
        LinkedHashSet<Integer> hashSet = new LinkedHashSet<>(nList);
        ArrayList<Integer> s = new ArrayList<>(hashSet);
        System.out.println(s);

    }

}