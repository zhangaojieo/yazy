package cn.yazy.集合.collection;


import java.util.ArrayList;
import java.util.Collection;

/**
 * 集合中存放的是元素的引用
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年10月20日上午9:57:57
 */
public class CollectionDemo3 {

    public static void main(String[] args) {

        Collection coll = new ArrayList();// 可以存储重复元素

        Point p =  new Point(1,2);

        coll.add(p);

        System.out.println(p);
        System.out.println(coll);

        p.setX(2);

        System.out.println(p);
        System.out.println(coll);

        System.out.println(p.getX());


    }
}