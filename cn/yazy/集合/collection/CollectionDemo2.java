package cn.yazy.集合.collection;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * 集合与元素equals方法相关的方法
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年10月20日上午9:22:26
 */
public class CollectionDemo2 {

    public static void main(String[] args) {
        //Collection coll = new ArrayList();//可以存储重复元素
        Collection coll = new HashSet();//元素不能重复存储
        coll.add(new Point(1,2));
        coll.add(new Point(3,4));
        coll.add(new Point(5,6));
        coll.add(new Point(7,8));
        coll.add(new Point(1,2));
        /*
         * 集合重写Object的toString()方法,输出格式为
         * [元素1.stString(),元素1.toString(),...]
         */
        System.out.println(coll);

        Point p = new Point(1,2);

        /*
         * 1.判断当前集合是否包含给定元素,这里判断的依据是给定元素是否与集合
         * 现有元素存在equals比较为true的情况
         * boolean contains(Object o)
         */
        boolean contains = coll.contains(p);
        System.out.println("包含:" + contains);//true

        /*
         * 2.remove用来从集合中删除给定元素,删除的也是与集合中equals比较
         * 为true的元素,注意:对于可以存放重复元素的集合而言,只删除一次
         * boolean remove(Object o)
         */
        coll.remove(p);
        System.out.println(coll);

    }

}
