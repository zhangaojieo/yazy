package cn.yazy.集合.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 泛型:JDK5之后推出的新特性
 * 泛型有称为参数化类型,允许我们在使用一个类时指定它里面属性的内容方法参数或返回值类,
 * 使得我们使用一个类可以更灵活
 * 泛型被广泛用于集合中,用来指定集合中的元素类型
 * 支持泛型的类在使用时如果未指定泛型,那么默认就是Object
 *
 * public interface Collection<E> ....{
 *
 * Collection<E>这里的<E>就是泛型
 *
 * Collection的add方法的定义参数为E
 *  boolean add(E e)
 *@author ZhangAJ:
 *@version 创建时间：2022年10月27日上午8:58:47
 */
public class CollectionDemo8 {

    public static void main(String[] args) {

        //创建集合指定泛型为String类型
        Collection<String> c = new ArrayList<>();
        //给集合添加元素
        c.add("JAVA");//编译器就会检查add方法的实际参数是否为String类型
        c.add("C++");
        c.add("C语言");
        c.add("python");
        //c.add(123);//编译错误,类型不符

        //遍历集合 迭代器也支持泛型 指定与要遍历的集合的泛型一致即可
        Iterator<String> it = c.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();//获取类型时无需再强制转换类型
            System.out.println(str);
        }

        System.out.println("=========");

        //增强for循环遍历
        for (String string : c) {
            System.out.println(string);
        }
    }

}
