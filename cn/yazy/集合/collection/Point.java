package cn.yazy.集合.collection;
/**
 *@author ZhangAJ:
 *@version 创建时间：2022年10月20日上午10:01:59
 */
public class Point {

    private int x;
    private int y;


    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public Point() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Point(int x, int y) {
        super();
        this.x = x;
        this.y = y;
    }




}