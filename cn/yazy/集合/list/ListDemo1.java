package cn.yazy.集合.list;

import java.util.ArrayList;
import java.util.List;

/**
 * List集合
 *    java.util.List接口,继承Collection
 *    list集合是可重复集,并且有序,提供了一套可以通过下标操作元素的方法
 * 常用实现类:
 *    java.util.ArrayList:内部使用数组实现,查询性能更好
 *    java.util.LinkedList:内部使用列表实现,首尾增删元素性能更好
 *@author ZhangAJ:
 *@version 创建时间：2022年10月31日下午2:14:47
 */
public class ListDemo1 {

    public static void main(String[] args) {

        //List集合的常见方法
        ////1.get()与set()
        List<String> list = new ArrayList<>();
        list.add("Java");
        list.add("C++");
        list.add("C语言");
        list.add("python");
        list.add("ios");
        /*
         * E get(int index);
         *  获取指定下标对应的元素
         */
        //获取第三个元素
        String e = list.get(2);
        System.out.println(e);//C语言

        System.out.println("==============================");

        for (int i = 0; i < list.size(); i++) {
            e = list.get(i);
            System.out.println(e);
        }

        /*
         *   E set(int index, E element);
         *	将给定元素设置到指定位置，返回值为该位置原有的元素。
         *	替换元素操作
         */
        String old = list.set(1, "C#");
        System.out.println("被替换的元素是： "+old);// C++
        System.out.println("==============================");
        System.out.println(list);//[JAVA, C#, C语言, Python, ios]



    }
}