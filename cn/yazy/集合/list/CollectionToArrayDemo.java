package cn.yazy.集合.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 集合转为数组
 * Collection提供了方法ToArray可以将当前集合转换为一个数组
 *
 * 数组转为集合
 * 数组的工具类Arrays提供了一个静态
 *@author ZhangAJ:
 *@version 创建时间：2022年10月31日下午3:11:47
 */
public class CollectionToArrayDemo {

    public static void main(String[] args) {

        //创建集合
        List<String> list = new ArrayList<>();
        //添加值
        list.add("Java");
        list.add("C++");
        list.add("C语言");
        list.add("python");
        list.add("ios");
        System.out.println(list);//[Java, C++, C语言, python, ios]

        /*
         * Object[] toArray();
         * 重载后toArray()方法要求传入一个数组,内部会将集合所有的元素存入改数组后
         * 将其返回(前提是集合的长度>=集合的size),如果给定的数组长度不足,
         * 则方法内部会自行根据给定数组类型创建一个与集合size一致长度的数组并将集合元素存入后返回
         */
        String[] array = list.toArray(new String[list.size()]);
        System.out.println(array.length);//5
        System.out.println(Arrays.toString(array));

        //数组转集合
        String[] arr = {"one","two","three","four","five"};
        //[one, two, three, four, five]
        System.out.println(Arrays.toString(arr));
        /**
         * public static <T> List<T> asList(T... a) {
         *  return new ArrayList<>(a);
         * }
         */
        List<String> list2 = Arrays.asList(arr);
        System.out.println(list2);//[one, two, three, four, five]

        //转换为集合后可以使用集合方法操作
        String str = list2.get(3);
        System.out.println(str);//four

        list2.set(1, "six");
        System.out.println(list2);//[one, six, three, four, five]

        //数组也跟着发生变化.注意:对数组转换的集合进行元素操作
        //就是对原数组对应数据的操作
        //[one, six, three, four, five]
        System.out.println(Arrays.toString(arr));

        /*
         * 	由于数组是定长的,因此对该集合进行增删元素的操作是不支持的
         * 	会抛出异常:
         * java.lang.UnsupportedOperationException
         *
         */

        //list2.add("seven");
        //System.out.println(list2);

        /*
         * 	若希望对集合进行增删操作,则需要自行创建一个集合,然后将该集合元素导入
         * 方式1:调用addAll()方法
         */
        List<String> list3 = new ArrayList<>();
        list3.addAll(list2);
        //在list2集合的基础上添加元素
        list3.add("seven");
        System.out.println(list3);//[one, six, three, four, five, seven]

        /*
         * 所有的集合都支持一个参数为Collection的构造方法,
         *   作用是在创建当前集合的同时包含给定集合中的所有元素
         * 方式2:调用集合的有参构造，以参数形式添加集合
         */
        List<String> list4 = new ArrayList<>(list2);
        System.out.println(list4);
        list4.add("ooo");
        //list4： [one, six, three, four, five, ooo]
        System.out.println("list4： " + list4);
    }

}