package cn.yazy.集合.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * java.util.Collections类
 * Collections是集合的工具类,里面定义了很多静态方法用于操作集合
 * Collections.sort(List List)方法,可以对List集合进行自然排序(从小到大)
 *@author ZhangAJ:
 *@version 创建时间：2022年11月3日上午8:41:42
 */
public class SortListDome1 {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        //创建随机数类
        Random ran = new Random();
        //给集合List随机存放10个整数
        for(int i=0;i<10;i++){
            list.add(ran.nextInt(100));
        }
        System.out.println("排序前:"+list);


        //对集合中的数据进行排序
        Collections.sort(list);

        System.out.println("排序后"+list);

    }
}