package cn.yazy.集合.list;

import java.util.LinkedList;

/**
 *     LinkedList类是List接口的实现类，这意味着它是一个list集合，可以根据
 * 索引来随机访问集合中的元素。除此之外，LinkedList还实现了Deque接口，
 * 因此它可以被当成双端列队来使用，自然也可以被当成“栈”来使用
 * LinkedList和ArrayList的实现机制完全不同，ArrarList内部以数组形式来
 * 保存结合中的元素，因此随机访问集合元素时有较好的性能，而LinkedList内部
 * 以链表的形式来保存集合中的元素，因此随机访问集合元素时性能较差，但在插入。
 * 删除元素时性能较好。
 *      对于所有的内部基于数组的集合实现，例如ArrayList，使用随机访问的性能
 * 比使用Iterator迭代访问的性能更好，因为随机访问回被映射成数组元素的访问
 *
 *
 *
 *      实际开发中对一个集合以元素的添加和删除经常涉及到首位操作，而LinkedList提供了
 * 大量的首位操作的方法，这些方法我们作为了解即可：
 *    public void addFirtst(E,e);将指定元素添加到此集合的开头
 *    public void addLast(E,e);将指定元素添加到此集合的结尾
 *    public E getFirst();返回此集合的第一个元素
 *    public E getLast();返回此集合的最后一个元素
 *    public E removeFirst();移除并返回此集合的第一个元素
 *    public E removeLast();移除并返回此集合的最后一个元素
 *    public E pop(E e);将元素推入此集合所表示的堆栈
 *    public boolean isEmpty();判断集合是否为空集  为空则返回true
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年11月3日上午8:53:26
 */
public class LinkedListDemo {

    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<String>();
        //添加元素
        list.addFirst("Java");
        list.addLast("C++");
        list.addFirst("Python");
        list.addLast("C#");
        list.add("C语言");
        System.out.println(list);//[Python, Java, C++, C#, C语言]
        //获取元素
        System.out.println(list.getFirst());//Python
        System.out.println(list.getLast());//C语言
        //删除元素
        System.out.println(list.removeFirst());//Python
        System.out.println(list.removeLast());//C语言

        //判断集合是否为空
        System.out.println(list.isEmpty());//false

        //弹出集合中栈顶的元素
        System.out.println(list.pop());//Java

        //将集合中的某个元素推到栈顶   相当于复制
        list.push("C++");
        System.out.println(list);//[C++, C++, C#]

    }
}