package cn.yazy.集合.list;

import java.util.ArrayList;
import java.util.List;

/**
 * List集合常用方法subList()方法
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年10月31日下午2:43:57
 */
public class ListDemo3 {

    public static void main(String[] args) {
        //创建List集合
        List<Integer> list = new ArrayList<>();
        //利用循环为集合赋值
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list);//[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

        /*
         * List<E> subList(int fromIndex, int toIndex);
         * 获取当前集合中指定范围内的子集
         * 俩个参数为开始与结束的下标(含头不含尾)
         */
        //获取上述集合中3-7
        List<Integer> subList = list.subList(3, 8);
        System.out.println(subList);//[3, 4, 5, 6, 7]

        //将子集每个元素扩大10倍
        for(int i=0;i<subList.size();i++) {
            subList.set(i,subList.get(i)*10);
        }
        System.out.println(subList);//[30, 40, 50, 60, 70]

        //对子集的操作就是对原集合对应元素的操作
        System.out.println(list);//[0, 1, 2, 30, 40, 50, 60, 70, 8, 9]

        //删除list集合中2-8
        list.subList(2, 9).clear();
        System.out.println(list);//[0, 1, 9]

    }
}