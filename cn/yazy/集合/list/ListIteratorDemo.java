package cn.yazy.集合.list;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 *Iterator接口(反向迭代):
 *与Collection提供的iterator()方法不同，List还提供了listIterator()方法，获取ListIterator对象，
 *ListIterator接口继承自Iterator接口，可以反向迭代。
 *方法:
 *   boolean hasPrevious()返回该迭代器关联的集合是否还有上一个元素。
 *  object previous()返回该迭代器的上一个元素。
 * void add() 在指定位置插入一个元素
 *@author ZhangAJ:
 *@version 创建时间：2022年11月7日下午2:43:33
 */
public class ListIteratorDemo {
    public static void main(String[] args) {
        //创建List集合
        List<String> list = new ArrayList<String>();
        //向集合中添加元素
        list.add("詹姆斯");
        list.add("库里");
        list.add("哈登");
        list.add("杜兰特");

        //获取迭代器ListIterator()
        ListIterator<String> lt =  list.listIterator();
        //正向迭代
        while (lt.hasNext()){
            System.out.println(lt.next());
            System.out.println(lt.nextIndex());
        }
        System.out.println("============");
        //反向迭代
        while (lt.hasPrevious()){
            System.out.println(lt.previous());
            System.out.println(lt.previousIndex());
        }

    }
}