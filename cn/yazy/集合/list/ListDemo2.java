package cn.yazy.集合.list;

import java.util.ArrayList;
import java.util.List;

/**
 * List集合重载的add()和remove()
 *
 *@author ZhangAJ:
 *@version 创建时间：2022年10月31日下午2:34:49
 */
public class ListDemo2 {

    public static void main(String[] args) {

        //List集合的常见方法
        List<String> list = new ArrayList<>();
        list.add("Java");
        list.add("C++");
        list.add("C语言");
        list.add("python");
        list.add("ios");
        /*
         * void add(int index, E element);
         * 将给定元素插入到指定位置
         */
        list.add(2,"two");
        System.out.println(list);//[Java, C++, two, C语言, python, ios]

        /*
         * E remove(int index);
         * 删除并返回指定位置上的元素
         */
        String remove = list.remove(2);
        System.out.println("被删除的元素是:" + remove);//two

        System.out.println(list);//[Java, C++, C语言, python, ios]

    }

}
