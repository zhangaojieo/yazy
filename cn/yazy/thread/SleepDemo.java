package cn.yazy.thread;
/*
sleep阻塞
线程提供了一个静态方法:
    static void sleep(long ms)
    使运行该方法的线程进入阻塞状态指定的毫秒,超时后线程会自动回到RUNNABLE状态等待再次获
取时间片并发运行.
 * @author: ZhangAJ
 * @create: 2022年12月05日 10:03
 */
public class SleepDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了!");
        try {
            Thread.sleep(5000);//主线程阻塞5秒钟
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("程序结束了!");
    }
}