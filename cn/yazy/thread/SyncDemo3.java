package cn.yazy.thread;

/*
    在静态方法上使用synchronized
        当在静态方法上使用synchronized后,该方法是一个同步方法.由于静态方法所属类,
      所以一定具有同步效果.
        静态方法使用的同步监视器对象为当前类的类对象(Class的实例).
 * @author: ZhangAJ
 * @create: 2022年12月05日 20:52
 */
public class SyncDemo3 {
    public static void main(String[] args) {
        Thread t1 = new Thread(){
            public void run(){
                Boo.dosome();
            }
        };
        Thread t2 = new Thread(){
            public void run(){
                Boo.dosome();
            }
        };
        t1.start();
        t2.start();
    }
}
class Boo{
    /**
     * synchronized在静态方法上使用时，指定的同步监视器对象为当前类的类对象。
     * 即:Class实例。
     * 在JVM中，每个被加载的类都有且只有一个Class的实例与之对应，后面讲反射
     * 知识点的时候会介绍类对象。
     */
//    public synchronized static void dosome(){
//        Thread t = Thread.currentThread();
//        try {
//            System.out.println(t.getName() + ":正在执行dosome方法...");
//            Thread.sleep(5000);
//            System.out.println(t.getName() + ":执行dosome方法完毕!");
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
    public static void dosome(){
        /*
        静态方法中使用同步块时，指定同步监视器对象通常还是用当前类的类对象
        获取方式为:类名.class
        */
        synchronized (Boo.class) {
            Thread t = Thread.currentThread();
            try {
                System.out.println(t.getName() + ":正在执行dosome方法...");
                Thread.sleep(5000);
                System.out.println(t.getName() + ":执行dosome方法完毕!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
