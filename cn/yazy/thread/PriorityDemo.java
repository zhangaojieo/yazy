package cn.yazy.thread;
/*
    线程优先级
        线程start后会纳入到线程调度器中统一管理,线程只能被动的被分配时间片并发运行,而无法主动索取时
     间片.线程调度器尽可能均匀的将时间片分配给每个线程.
   线程有10个优先级,使用整数1-10表示
      1为最小优先级,10为最高优先级.5为默认值
      调整线程的优先级可以最大程度的干涉获取时间片的几率.优先级越高的线程获取时间片的次数越
    多,反之则越少.
 * @author: ZhangAJ
 * @create: 2022年12月05日 9:55
 */
public class PriorityDemo {
    //该类有4个线程   main方法为主线程
    public static void main(String[] args) {
        Thread max = new Thread(){
            public void run(){
                for(int i=0;i<10000;i++){
                    System.out.println("max");
                }
            }
        };
        Thread min = new Thread(){
            public void run(){
                for(int i=0;i<10000;i++){
                    System.out.println("min");
                }
            }
        };
        Thread norm = new Thread(){
            public void run(){
                for(int i=0;i<10000;i++){
                    System.out.println("norm");
                }
            }
        };
        min.setPriority(Thread.MIN_PRIORITY);
        max.setPriority(Thread.MAX_PRIORITY);
        min.start();//优先级最低 1
        norm.start();//优先级默认 5
        max.start();//优先级最高 10
    }
}