package cn.yazy.thread;
/**
 * 使用匿名内部类完成线程的两种创建方式
 * @author: ZhangAJ
 * @create: 2022年12月01日 9:30
 */
public class ThreadDemo3 {
    public static void main(String[] args) {
        //第一种
        Thread t1 = new Thread(){
            public void run(){
                for (int i=0;i<1000;i++){
                    System.out.println("你是谁啊?");
                }
            }
        };

        //第二种
        Runnable r2 = new Runnable(){
            public void run(){
                for (int i=0;i<1000;i++){
                    System.out.println("开门!查水表的!");
                }
            }
        };
        Thread t2 = new Thread(r2);

        t1.start();
        t2.start();
    }
}