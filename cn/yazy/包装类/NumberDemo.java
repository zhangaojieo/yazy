package cn.yazy.包装类;
/**
 * java提供了8个包装类,目的是为了解决基本数据类型不能直接参与面向对象开发的问题,使得基本类型可以通过包装类的
 * 实例对象形式存在,其中有关于数字类的包装类都继承于java.lang.Number,而char和boolean的包装类都直接继承
 * 了Object.
 *@author ZhangAJ:
 *@version 创建时间：2022年9月26日下午3:04:01
 */
public class NumberDemo {

    public static void main(String[] args) {
        /*
         * Number 是一个抽象类,定义了一些方法,目的就是包装类可以将其表示的基本数据类型转换为其他数据类型
         */
        //基本的包装类 Integer Double Float Short
        //Long Byte Boolean Character

        //1.基本数据类型转换为包装类
        int i = 123;
        //java推荐我们使用包装类的静态方法valueOf()
        //将基本数据类型转换为包装类,而不是直接去new

        //Integer 会重用-128-127之间的整数对象
        Integer i1 = Integer.valueOf(i);
        Integer i2 = Integer.valueOf(i);

        System.out.println(i1==i2);//true
        System.out.println(i1.equals(i2));//true


        double d = 123.456;
        //Double类型不会直接转换,而是直接new
        Double d1 = Double.valueOf(d);
        Double d2 = Double.valueOf(d);

        System.out.println(d1==d2);//false
        System.out.println(d1.equals(d2));//true

        //包装类型转换为基本类型,
        //使用intValue() doubleValue() ...xxValue()方法
        int value = i1.intValue();
        System.out.println(value);//123

        double value2 = d1.doubleValue();
        System.out.println(value2);//123.456

        //大类型转小类型可能存在丢失精度
        int value3 = d1.intValue();
        System.out.println(value3);//123


        //1.可以通过包装类的属性MAX_VALUE和MIN_VALUE来获取最大取值以及最小取值
        int maxValue = Integer.MAX_VALUE;
        int minValue = Integer.MIN_VALUE;
        System.out.println("int类型的最大取值:"+maxValue);//2147483647
        System.out.println("int类型的最小取值:"+minValue);//-2147483648

        //练习:自己完成double的最大与最小取值
        double maxValue1 = Double.MAX_VALUE;
        double minValue1 = Double.MIN_VALUE;
        System.out.println("double类型的最大取值:"+maxValue1);//1.7976931348623157E308
        System.out.println("double类型的最小取值:"+minValue1);//4.9E-324
        //练习:自己完成float的最大与最小取值
        float maxValue2 = Float.MAX_VALUE;
        float minValue2 = Float.MIN_VALUE;
        System.out.println("float类型的最大取值:"+maxValue2);//3.4028235E38
        System.out.println("float类型的最小取值:"+minValue2);//1.4E-45
        //练习:自己完成byte的最大与最小取值
        byte maxValue3 = Byte.MAX_VALUE;
        byte minValue3 = Byte.MIN_VALUE;
        System.out.println("byte类型的最大取值:"+maxValue3);//127
        System.out.println("byte类型的最小取值:"+minValue3);//-128
        //练习:自己完成short的最大与最小取值
        short maxValue4 = Short.MAX_VALUE;
        short minValue4 = Short.MIN_VALUE;
        System.out.println("short类型的最大取值:"+maxValue4);//32767
        System.out.println("short类型的最小取值:"+minValue4);//-32768
        //练习:自己完成long的最大与最小取值
        long maxValue5 = Long.MAX_VALUE;
        long minValue5 = Long.MIN_VALUE;
        System.out.println("long类型的最大取值:"+maxValue5);//32767
        System.out.println("long类型的最小取值:"+minValue5);//-32768

        //2.字符串转换为基本数据类型的前提是该字符串正确的描述了基本数据类型可以保存的值,可以利用parseXX()
        //方法.否则会抛出异常:java.lang.NumberFormatException
        String str = "123";
        int parseInt = Integer.parseInt(str);
        System.out.println(str);//"123"
        System.out.println(parseInt);//123
        //字符串拼接
        System.out.println(str+parseInt);//123123
        //数字加法
        System.out.println(parseInt+parseInt);//246

        String str2 = "123.123";
        //不能转换 会发生转换异常
        //String str2 = "123.123哈哈哈";
        double parseDouble = Double.parseDouble(str2);
        System.out.println(parseDouble);//123.123


        //从JDK 5 开始引入自动拆箱
        Integer integer = new Integer("123");

        int intValue = integer.intValue();
        //以上代码会触发自动拆箱的特性,编译器会补充代码,将包装类型转换为基本类型
        int inte = new Integer("123");//自动拆箱

        //触发编译器的自动装箱特性
        Integer integer2 = Integer.valueOf(123);
        //代码简写(代码会被编译器改为)
        Integer in = 123;


    }

}
